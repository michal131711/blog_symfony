<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Faker\Factory;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UsersRolesFixtures extends Fixture
{
    private $passwordEncoder;
    protected $faker;

    /**
     * UsersRolesFixtures constructor.
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->faker = Factory::create();
    }

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setUsername('Administrator');
        $user->setEmail('mm@perq.pl');
        $user->setRoles(['roles' => 'ROLE_ADMIN']);
        $hash = $this->passwordEncoder->encodePassword($user, 'testowe1');
        $user->setPassword($hash);
        $user->setTermsAccepted(true);
        $manager->persist($user);
        $manager->flush();
    }
}
