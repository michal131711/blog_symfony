<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class CategoryFixtures extends Fixture
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $listsCategory = ['JavaScript', 'Angular', 'HTML', 'PHP', 'Ruby',
            'Python', 'Java', '.Net', 'Scala', 'C , C++', 'Android', 'IOS',
            'Testing', 'UX/UI', 'Game', 'Data', 'Security', 'SAP'];
        foreach ($listsCategory as $v => $value) {
            $category = new Category();

            $category->setName($value);
            $manager->persist($category);

            $manager->flush();
        }
    }
}
