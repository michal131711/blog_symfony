<?php

namespace App\DataFixtures;

use App\Entity\Country;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class CountryFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $listsCountry = ['Niemcy', 'Anglia', 'Polska', 'Indie'];
        foreach ($listsCountry as $v => $value) {
            $country = new Country();

            $country->setName($value);
            $manager->persist($country);

            $manager->flush();
        }
    }
}
