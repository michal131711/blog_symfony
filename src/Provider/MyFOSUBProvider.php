<?php
/**
 * Created by IntelliJ IDEA.
 * User: Cassiopeia
 * Date: 11.11.2018
 * Time: 15:40
 */

namespace App\Provider;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use KnpU\OAuth2ClientBundle\Client\Provider\FacebookClient;
use KnpU\OAuth2ClientBundle\Security\Authenticator\SocialAuthenticator;
use League\OAuth2\Client\Provider\FacebookUser;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class MyFOSUBProvider extends SocialAuthenticator
{
    private $clientRegistry;
    private $em;
    private $passwordEncoder;

    public function __construct(
        ClientRegistry $clientRegistry,
        EntityManagerInterface $em,
        UserPasswordEncoderInterface $passwordEncoder
    ) {
        $this->clientRegistry = $clientRegistry;
        $this->em = $em;
        $this->passwordEncoder = $passwordEncoder;
    }

    public function supports(Request $request)
    {
        return $request->attributes->get('_route') === 'connect_facebook_check';
    }

    public function getCredentials(Request $request)
    {
        return $this->fetchAccessToken($this->getFacebookClient());
    }

    /**
     * @return \KnpU\OAuth2ClientBundle\Client\OAuth2Client|FacebookClient
     */
    private function getFacebookClient()
    {
        return $this->clientRegistry
            ->getClient('facebook');
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        /** @var FacebookUser $facebookUser */
        $facebookUser = $this->getFacebookClient()
            ->fetchUserFromToken($credentials);

        $email = $facebookUser->getEmail();

        $existingUser = $this->em->getRepository(User::class)
            ->findOneBy(['facebook_id' => $facebookUser->getId()]);
        if ($existingUser) {
            return $existingUser;
        }

        $user = $this->em->getRepository(User::class)
            ->findOneBy(['email' => $email]);
        if (isset($user)) {
            $user->setFacebookId($facebookUser->getId());
            $this->em->persist($user);
            $this->em->flush();
        } else {
            $createUser = new User();
            $createUser->setUsername($facebookUser->getName());
            $createUser->setEmail($facebookUser->getEmail());
            $createUser->setRoles(['roles' => 'ROLE_USER']);
            $hash = $this->passwordEncoder->encodePassword($createUser, $facebookUser->getId());
            $createUser->setPassword($hash);
            $random = random_int(7777733331111, 9999933331111);
            $createUser->setToken((string)$random);
            $createUser->setTermsAccepted(1);
            $this->em->persist($createUser);
            $this->em->flush();
            return $createUser;
        }
        return $user;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        if ($token->getUser()->getFacebookId() == null) {
            return new RedirectResponse('/account');
        }
        return new RedirectResponse('/');
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $message = strtr($exception->getMessageKey(), $exception->getMessageData());

        return new Response($message, Response::HTTP_FORBIDDEN);
    }

    /**
     * Called when authentication is needed, but it's not sent.
     * This redirects to the 'login'.
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        if (!$request->headers->has('Authorization')) {
            throw new AuthenticationException();
        }
        return new RedirectResponse(
            '/login/',
            Response::HTTP_TEMPORARY_REDIRECT
        );
    }
}
