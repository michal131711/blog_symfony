<?php
/**
 * Created by IntelliJ IDEA.
 * User: Cassiopeia
 * Date: 12.11.2018
 * Time: 15:18
 */

namespace App\Provider;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Registry;
use FOS\UserBundle\Model\UserManagerInterface;
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\Security\Core\User\FOSUBUserProvider as BaseClass;

class FOSUBUserProvider extends BaseClass
{
    private $doctrine;

    /**
     * @param UserManagerInterface $userManager
     * @param array $properties
     * @param Registry $doctrine
     */
    public function __construct(UserManagerInterface $userManager, array $properties, $doctrine)
    {
        parent::__construct($userManager, $properties);

        $this->doctrine = $doctrine;
    }

    /**
     * {@inheritdoc}
     */
    public function loadUserByOAuthUserResponse(UserResponseInterface $response)
    {
        $username = $response->getUsername();
        $property = $this->getProperty($response);

        $user = $this->userManager->findUserBy(array($this->getProperty($response) => $username));

        $email = $response->getEmail();
        // check if we already have this user
        $existing = $this->userManager->findUserBy(array('email' => $email));
        if ($existing instanceof User) {
            // in case of Facebook login, update the facebook_id
            if ($property == "facebook_id") {
                $existing->setFacebookId($username);
            }
            // in case of Google login, update the google_id
            if ($property == "googleId") {
                $existing->setGmailID($username);
            }
            $this->userManager->updateUser($existing);

            return $existing;
        }

        // if we don't know the user, create it
        if (null === $user || null === $username) {
            /** @var User $user */
            $user = $this->userManager->createUser();
            $nick = "michal131716@wp.pl"; // to be changed

            $user->setUpdatedAt(new \DateTime());
            $user->setFacebookAccessToken(true);

            $user->setEmail($nick);
            $user->setPassword(sha1(uniqid()));
            $user->getRoles();

            if ($property == "facebook_id") {
                $user->setFacebookId($username);
            }
            if ($property == "googleId") {
                $user->setGoogleId($username);
            }
        }

        $user->setEmail($response->getEmail());

        $this->userManager->updateUser($user);

        return $user;
    }
}
