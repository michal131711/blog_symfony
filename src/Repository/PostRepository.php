<?php

namespace App\Repository;

use App\Entity\Post;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Post|null find($id, $lockMode = null, $lockVersion = null)
 * @method Post|null findOneBy(array $criteria, array $orderBy = null)
 * @method Post[]    findAll()
 * @method Post[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Post::class);
    }

    public function search($value, $citySearch)
    {
        return $this->createQueryBuilder('p')
            ->select('p')
            ->where('p.title LIKE :title')
            ->andWhere('p.city LIKE :city')
            ->setParameter('title', '%' . $value . '%')
            ->setParameter('city', '%' . $citySearch . '%')
            ->getQuery()
            ->getResult();
    }

    public function findAllTitle()
    {
        return $this->createQueryBuilder('p')
            ->select('p.title')
            ->getQuery()
            ->getResult();
    }

    public function myOfferAll($user_id)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.user = :user_id')
            ->setParameter('user_id', $user_id)
            ->getQuery()
            ->getResult();
    }

    public function findAllPosts()
    {
        return $this->findBy(array(), array('createdAt' => 'DESC'));
    }


}
