<?php

namespace App\Repository;

use App\Entity\NotificationMessage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method NotificationMessage|null find($id, $lockMode = null, $lockVersion = null)
 * @method NotificationMessage|null findOneBy(array $criteria, array $orderBy = null)
 * @method NotificationMessage[]    findAll()
 * @method NotificationMessage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NotificationMessageRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, NotificationMessage::class);
    }

    // /**
    //  * @return NotificationMessage[] Returns an array of NotificationMessage objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?NotificationMessage
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    public function allApplicationsNotifyByUser($user_id)
    {
        return $this->createQueryBuilder('p')
            ->leftJoin('p.notification','notification')
            ->andWhere('notification.user = :user_id')
            ->andWhere('p.redOffer = false')
            ->setParameter('user_id', $user_id)
            ->getQuery()
            ->getResult();
    }

    public function countNotificationByUser($user_id)
    {
        return $this->createQueryBuilder('p')
            ->select('count(p.id)')
            ->leftJoin('p.notification','notification')
            ->andWhere('notification.user = :user_id')
            ->andWhere('p.redOffer = false')
            ->setParameter('user_id', $user_id)
            ->getQuery()
            ->getSingleScalarResult();
    }
}
