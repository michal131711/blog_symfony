<?php
/**
 * Created by IntelliJ IDEA.
 * User: micha
 * Date: 03.12.18
 * Time: 10:02
 */

namespace App\Traits;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Form;

trait SearchPostTrait
{
    public function searchPosts(): Form
    {
        $form =
            $this->createFormBuilder(null)
                ->setAction($this->generateUrl('title_search', [], true))
                ->add("Search", TextType::class, [
                   'attr' => array('class' => 'm-2'),
                    'required' => false,
                    ])
                ->add("Search_city", TextType::class, [
                    'attr' => array('class' => 'm-2'),
                    'required' => false,
                    ])
                ->add("Szukaj_oferty", SubmitType::class)
                ->getForm();
        return $form;
    }

}
