<?php
/**
 * Created by IntelliJ IDEA.
 * User: micha
 * Date: 03.12.18
 * Time: 11:11
 */

namespace App\Traits;

use Symfony\Component\HttpFoundation\Request;

trait PaginatePostTrait
{
    public function paginatePosts($resultPrintPaginate, $paginator, Request $request)
    {
        $posts = $paginator
            ->paginate(
                $resultPrintPaginate, /* query NOT result */
                $request
                    ->query
                    ->getInt('page', $request->get('page') ? $request->get('page') : 1),
                $request
                    ->query
                    ->getInt('limit', 20)
            );
        return $posts;
    }
}
    // public function jakasNazwaFn(\stdClass $params): array
    //     $offset = ($params->page - 1) * $params->limit;
    //     $qb = $this->createQueryBuilder('p');
    //     if ($params->title) {
    //         $qb->where('p.title like :title');
    //         $qb->setParameter('title', $params->title . '%');
    //     }
    //     $qb->setFirstResult($offset);
    //     if ($params->limit) {
    //         $qb->setMaxResults($params->limit);
    //     }
    //     return $qb->getQuery()->getResult();
    // }
