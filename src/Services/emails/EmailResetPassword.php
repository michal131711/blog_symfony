<?php
/**
 * Created by IntelliJ IDEA.
 * User: micha
 * Date: 09.12.18
 * Time: 18:55
 */

namespace App\Services\emails;

use Swift_Message;

class EmailResetPassword
{
    /**
     * @param $title
     * @param $address
     * @param $sendToAdress
     * @param $bodyMessage
     * @return Swift_Message|null
     */
    public function sendResetEmail($title, $address, $sendToAddress, $bodyMessage, $contentType): ?Swift_Message
    {
        return (new Swift_Message($title))
            ->setFrom($address)
            ->setTo($sendToAddress)
            ->setBody($bodyMessage, $contentType);
    }
}
