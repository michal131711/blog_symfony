<?php
/**
 * Created by IntelliJ IDEA.
 * User: micha
 * Date: 09.12.18
 * Time: 18:55
 */

namespace App\Services\emails;

use Swift_Attachment;
use Swift_Message;

class EmailSendApplications
{
    /**
     * @param $title
     * @param $address
     * @param $sendToAddress
     * @param $bodyMessage
     * @param $contentType
     * @param null $fileAttache
     * @return Swift_Message|null
     */
    public function sendApplicationOnOffer(
        $title,
        $address,
        $sendToAddress,
        $bodyMessage,
        $contentType,
        $fileAttache = null
    ): ?Swift_Message {
        return (new Swift_Message($title))
            ->setFrom($address)
            ->setTo($sendToAddress)
            ->setBody($bodyMessage, $contentType)
            ->attach(Swift_Attachment::fromPath($fileAttache));
    }
}
