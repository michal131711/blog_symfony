<?php
/**
 * Created by IntelliJ IDEA.
 * User: micha
 * Date: 03.02.19
 * Time: 16:36
 */

namespace App\Services\notifications;


use App\Entity\Notification;
use App\Entity\NotificationMessage;
use App\Repository\NotificationMessageRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class NotificationService
{
    private $managerRegistry;
    private $notificationMessageRepository;
    private $objectNormalizer;
    private $notification;

    /**
     * CategoryService constructor.
     * @param Notification $notification
     * @param EntityManagerInterface $managerRegistry
     * @param NotificationMessageRepository $notificationMessageRepository
     * @param ObjectNormalizer $objectNormalizer
     */
    public function __construct(
        Notification $notification,
        EntityManagerInterface $managerRegistry,
        NotificationMessageRepository $notificationMessageRepository,
        ObjectNormalizer $objectNormalizer
    ) {
        $this->managerRegistry = $managerRegistry;
        $this->notificationMessageRepository = $notificationMessageRepository;
        $this->objectNormalizer = $objectNormalizer;
        $this->notification = $notification;
    }

    public function findAllApplicationsNotifyByUser($notifyUser)
    {
        return $this->notificationMessageRepository->allApplicationsNotifyByUser($notifyUser);
    }

    public function countAllApplicationsNotifyByUser($notifyUser)
    {
        return $this->notificationMessageRepository->countNotificationByUser($notifyUser);
    }

    public function findNotificationById($id)
    {
        return $this->managerRegistry->getRepository(Notification::class)->find($id);
    }

    public function findNotificationMessageById($id)
    {
        return $this->managerRegistry->getRepository(NotificationMessage::class)->find($id);
    }

    public function saveNotify(): EntityManagerInterface
    {
        return $this->managerRegistry;
    }
}
