<?php
/**
 * Created by IntelliJ IDEA.
 * User: micha
 * Date: 20.12.18
 * Time: 17:42
 */

namespace App\Services\posts;

use App\Entity\Post;
use App\Repository\CategoryRepository;
use App\Repository\PostRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class PostService
{
    private $managerRegistry;
    private $postRepository;
    private $categoryRepository;
    private $objectNormalizer;
    private $post;

    /**
     * CategoryService constructor.
     * @
     * @param EntityManagerInterface $managerRegistry
     * @param PostRepository $postRepository
     * @param ObjectNormalizer $objectNormalizer
     */
    public function __construct(
        Post $post,
        EntityManagerInterface $managerRegistry,
        CategoryRepository $categoryRepository,
        PostRepository $postRepository,
        ObjectNormalizer $objectNormalizer
    ) {
        $this->managerRegistry = $managerRegistry;
        $this->postRepository = $postRepository;
        $this->categoryRepository = $categoryRepository;
        $this->objectNormalizer = $objectNormalizer;
        $this->post = $post;
    }

    /**
     * @return array
     */
    public function getAllPost(): array
    {
        return $this->managerRegistry->getRepository(Post::class)->findAllPosts();
    }

    public function titleSearch($_query, $_citySearchQuery)
    {
        return $this->postRepository->search($_query, $_citySearchQuery);
    }

    /**
     * @return Serializer
     */
    public function resultFindPosts(): Serializer
    {
        $this->objectNormalizer->setCircularReferenceLimit(1);
        $this->objectNormalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });
        $normalizers = array($this->objectNormalizer);
        $encoders = array(new XmlEncoder(), new JsonEncoder());

        $serializer = new Serializer($normalizers, $encoders);
        return $serializer;
    }

    public function createPost(): EntityManagerInterface
    {
        return $this->managerRegistry;
    }

    public function findPostById($id)
    {
        return $this->managerRegistry->getRepository(Post::class)->find($id);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function findCategoryById($id)
    {
        return $this->categoryRepository->findCategoryById($id);
    }

    public function updatePost(): EntityManagerInterface
    {
        return $this->managerRegistry;
    }

    public function removePost($removeRecord): EntityManagerInterface
    {
        $this->managerRegistry->remove($removeRecord);
        return $this->managerRegistry;
    }

    public function findAllUserOffer($postsUser)
    {
        return $this->postRepository->myOfferAll($postsUser);
    }
}
