<?php
/**
 * Created by IntelliJ IDEA.
 * User: micha
 * Date: 22.12.18
 * Time: 10:19
 */

namespace App\Services\users;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserService
{
    private $userPasswordEncoder;
    private $managerRegistry;
    private $user;

    /**
     * @return EntityManagerInterface
     */
    public function getManagerRegistry(): EntityManagerInterface
    {
        return $this->managerRegistry;
    }

    /**
     * CategoryService constructor.
     * @
     * @param EntityManagerInterface $managerRegistry
     * @param UserPasswordEncoderInterface $userPasswordEncoder
     */
    public function __construct(
        EntityManagerInterface $managerRegistry,
        UserPasswordEncoderInterface $userPasswordEncoder,
        User $user
    ) {
        $this->managerRegistry = $managerRegistry;
        $this->userPasswordEncoder = $userPasswordEncoder;
        $this->user = $user;
    }

    public function getAllUsers()
    {
        return $this->managerRegistry->getRepository(User::class)->findAll();
    }

    public function findUserByName(array $name)
    {
        return $this->managerRegistry->getRepository(User::class)->findOneBy($name);
    }
}
