<?php
/**
 * Created by IntelliJ IDEA.
 * User: micha
 * Date: 07.12.18
 * Time: 16:55
 */

namespace App\Services\files;

use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploaderService
{
    private $targetDirectory;

    /**
     * FileUploaderService constructor.
     * @param $targetDirectory
     */
    public function __construct($targetDirectory)
    {
        $this->targetDirectory = $targetDirectory;
    }

    /**
     * @var UploadedFile $file
     * @return string|null
     */
    public function upload(UploadedFile $file): ?string
    {
        $fileName = $this->generateUniqueFileName() . '_' . $file->getClientOriginalName();
        try {
            $file->move($this->getTargetDirectory(), $fileName);
        } catch (FileException $e) {
            $e->getMessage();
        }
        return $fileName;
    }

    /**
     * @return string
     */
    private function generateUniqueFileName()
    {
        return md5(uniqid('', true));
    }

    public function getTargetDirectory()
    {
        return $this->targetDirectory;
    }
}
