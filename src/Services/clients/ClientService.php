<?php
/**
 * Created by IntelliJ IDEA.
 * User: micha
 * Date: 20.12.18
 * Time: 19:09
 */

namespace App\Services\clients;

use App\Entity\Client;
use App\Entity\Post;
use App\Services\emails\EmailSendApplications;
use App\Services\files\FileUploaderService;
use App\Services\posts\PostService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Swift_Mailer;

class ClientService
{
    private $emailSendApplications;
    private $managerRegistry;
    private $objectNormalizer;
    private $fileUploaderService;
    private $client;
    private $mailer;
    private $post;
    private $postService;

    /**
     * CategoryService constructor.
     * @
     * @param Client $client
     * @param Post $post
     * @param PostService $postService
     * @param FileUploaderService $fileUploaderService
     * @param EntityManagerInterface $managerRegistry
     * @param \App\Services\clients\EmailSendApplications $emailSendApplications
     * @param ObjectNormalizer $objectNormalizer
     * @param Swift_Mailer $mailer
     */
    public function __construct(
        Client $client,
        Post $post,
        PostService $postService,
        FileUploaderService $fileUploaderService,
        EntityManagerInterface $managerRegistry,
        EmailSendApplications $emailSendApplications,
        ObjectNormalizer $objectNormalizer,
        Swift_Mailer $mailer
    ) {
        $this->emailSendApplications = $emailSendApplications;
        $this->managerRegistry = $managerRegistry;
        $this->objectNormalizer = $objectNormalizer;
        $this->client = $client;
        $this->fileUploaderService = $fileUploaderService;
        $this->post = $post;
        $this->postService = $postService;
        $this->mailer = $mailer;
    }

    /**
     * @param $templateMailing
     * @param $saveDataClass
     * @return FileUploaderService
     */
    public function createApplicationClient(
        $postTitle,
        $clientEmail,
        $sendToEmail,
        $templateMailing,
        $fileSaveName
    ): EntityManagerInterface {
        $message= $this->emailSendApplications->sendApplicationOnOffer(
            'Aplikacja na stanowisko: ' . $postTitle,
            $clientEmail,
            $sendToEmail,
            $templateMailing,
            'text/html',
            $this->fileUploaderService->getTargetDirectory().'/'.$fileSaveName
        );
        $this->mailer->send($message);
        $this->managerRegistry->persist($this->client);
        return $this->managerRegistry;
    }

    public function findClientByName(array $name)
    {
        return $this->managerRegistry->getRepository(Client::class)->findOneBy($name);
    }

    public function getAllClient()
    {
        return $this->managerRegistry->getRepository(Client::class)->findAll();
    }
}
