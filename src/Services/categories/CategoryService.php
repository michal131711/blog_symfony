<?php
/**
 * Created by IntelliJ IDEA.
 * User: micha
 * Date: 20.12.18
 * Time: 17:20
 */

namespace App\Services\categories;

use App\Entity\Category;
use Doctrine\ORM\EntityManagerInterface;

class CategoryService
{
    private $managerRegistry;

    /**
     * CategoryService constructor.
     * @
     * @param EntityManagerInterface $managerRegistry
     */
    public function __construct(EntityManagerInterface $managerRegistry)
    {
        $this->managerRegistry = $managerRegistry;
    }

    /**
     * @return array
     */
    public function getAllCategories(): array
    {
        return $this->managerRegistry->getRepository(Category::class)->findAll();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function findCategoryById($id)
    {
        return $this->managerRegistry->getRepository(Category::class)->find($id);
    }

    public function findCategoryByName(array $name)
    {
        return $this->managerRegistry->getRepository(Category::class)->findOneBy([$name]);
    }

    public function createCategory($category): EntityManagerInterface
    {
        $this->managerRegistry->persist($category);
        return $this->managerRegistry;
    }

    public function updateCategory(): EntityManagerInterface
    {
        return $this->managerRegistry;
    }

    public function removeCategory($category): EntityManagerInterface
    {
        $this->managerRegistry->remove($category);
        return $this->managerRegistry;
    }
}
