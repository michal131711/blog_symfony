<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\NotificationMessageRepository")
 */
class NotificationMessage
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Notification", inversedBy="notificationMessage")
     */
    private $notification;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @var bool
     * @ORM\Column(type="boolean", name="red_offer")
     */
    private $redOffer;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Notification
     */
    public function setName($name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return bool
     */
    public function getRedOffer(): ?bool
    {
        return $this->redOffer;
    }

    /**
     * @param bool $redOffer
     * @return bool
     */
    public function setRedOffer(bool $redOffer= true)
    {
        return $this->redOffer = $redOffer;
    }

    /**
     * @return mixed
     */
    public function getNotification()
    {
        return $this->notification;
    }

    /**
     * @param Notification $notification
     */
    public function setNotification($notification): void
    {
        $this->notification = $notification;
    }

    /**
     * @param Notification $notification
     * @return NotificationMessage
     */
    public function addNotification(Notification $notification): self
    {
        if (!$this->notification->contains($notification)) {
            $this->notification[] = $notification;
        }

        return $this;
    }

    /**
     * @param Notification $notification
     * @return NotificationMessage
     */
    public function removeNotification(Notification $notification): self
    {
        if ($this->notification->contains($notification)) {
            $this->notification->removeElement($notification);
        }

        return $this;
    }
}
