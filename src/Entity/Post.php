<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PostRepository")
 */
class Post
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank(message="Należy wpisać tytuł oferty")
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @Assert\NotBlank(message="Należy wpisać nazwę firmy")
     * @ORM\Column(type="string", length=255)
     */
    private $companyName;

    /**
     * @Assert\NotBlank(message="Należy podać wymaganą specjalność")
     * @ORM\Column(type="string", length=255)
     */
    private $nameSpecializations;
    /**
     * @Assert\NotBlank(message="Prosze podać widełki ewntualnie w opisie dodać szczegóły")
     * @ORM\Column(type="decimal", precision=7, scale=2)
     */
    private $priceOffer;

    /**
     * @Assert\NotBlank(message="Proszę podać kraj")
     * @ORM\ManyToOne(targetEntity="App\Entity\Country", inversedBy="posts")
     */
    private $country;

    /**
     * @Assert\NotBlank(message="Proszę podać Miejscowość")
     * @ORM\Column(type="string", length=220)
     */
    private $city;

    /**
     * @Assert\NotBlank(message="Proszę podać adres firmy.")
     * @ORM\Column(type="string", length=255)
     */
    private $address;

    /**
     * @Assert\NotBlank(message="Proszę podać numer budynku.")
     * @ORM\Column(type="string", length=50)
     */
    private $numberBuild;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $numberHome;

    /**
     * @Assert\NotBlank(message="Proszę podać kod pocztowy")
     * @ORM\Column(type="string", length=100)
     */
    private $postCode;

    /**
     * @Assert\NotBlank(message="Proszę opisać w szczegółach swoją ofertę pracy dla ludzi")
     * @ORM\Column(type="text", length=255)
     */
    private $body;

    /**
     * @Assert\NotBlank(message="Proszę podać nazwę waluty")
     * @ORM\Column(type="text", length=100)
     */
    private $currency;

    /**
     * @Assert\NotBlank(message="Proszę wpisać nr. kontaktowy")
     * @ORM\Column(type="string", length=150)
     */
    private $contactPhone;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $contactEmail;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     */
    private $user;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Category", inversedBy="posts")
     */
    private $category;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Client", mappedBy="posts")
     */
    private $client;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Notification", mappedBy="post")
     */
    private $notification;


    public function __construct()
    {
        $this->country = new ArrayCollection();
        $this->category = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param integer $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return string|null
     */
    public function getNameSpecializations(): ?string
    {
        return $this->nameSpecializations;
    }

    /**
     * @param string $nameSpecializations
     */
    public function setNameSpecializations($nameSpecializations): void
    {
        $this->nameSpecializations = $nameSpecializations;
    }
    /**
     * @return string|null
     */
    public function getCompanyName(): ?string
    {
        return $this->companyName;
    }

    /**
     * @param string $companyName
     */
    public function setCompanyName($companyName): void
    {
        $this->companyName = $companyName;
    }

    /**
     * @return string|null
     */
    public function getBody(): ?string
    {
        return $this->body;
    }

    /**
     * @param string $body
     */
    public function setBody($body): void
    {
        $this->body = $body;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param Category $category
     */
    public function setCategory($category): void
    {
        $this->category = $category;
    }

    /**
     * @param Category $category
     * @return Post
     */
    public function addCategory(Category $category): self
    {
        if (!$this->category->contains($category)) {
            $this->category[] = $category;
        }

        return $this;
    }

    /**
     * @param Category $category
     * @return Post
     */
    public function removeCategory(Category $category): self
    {
        if ($this->category->contains($category)) {
            $this->category->removeElement($category);
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry($country): void
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity($city): void
    {
        $this->city = $city;
    }

    /**
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress($address): void
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getNumberBuild(): ?string
    {
        return $this->numberBuild;
    }

    /**
     * @param string $numberBuild
     */
    public function setNumberBuild($numberBuild): void
    {
        $this->numberBuild = $numberBuild;
    }

    /**
     * @return string
     */
    public function getNumberHome(): ?string
    {
        return $this->numberHome;
    }

    /**
     * @param string $numberHome
     */
    public function setNumberHome($numberHome): void
    {
        $this->numberHome = $numberHome;
    }

    /**
     * @return string
     */
    public function getPostCode(): ?string
    {
        return $this->postCode;
    }

    /**
     * @param string $postCode
     */
    public function setPostCode($postCode): void
    {
        $this->postCode = $postCode;
    }

    /**
     * @return double|null
     */
    public function getPriceOffer(): ?float
    {
        return $this->priceOffer;
    }

    /**
     * @param double $priceOffer
     */
    public function setPriceOffer($priceOffer): void
    {
        $this->priceOffer = $priceOffer;
    }

    /**
     * @return string
     */
    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency($currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return string
     */
    public function getContactPhone(): ?string
    {
        return $this->contactPhone;
    }

    /**
     * @param string $contactPhone
     */
    public function setContactPhone($contactPhone): void
    {
        $this->contactPhone = $contactPhone;
    }

    /**
     * @return string
     */
    public function getContactEmail(): ?string
    {
        return $this->contactEmail;
    }

    /**
     * @param string $contactEmail
     */
    public function setContactEmail($contactEmail): void
    {
        $this->contactEmail = $contactEmail;
    }

    /**
     * @return mixed
     */
    public function getNotification()
    {
        return $this->client;
    }

    /**
     * @param mixed $notification
     */
    public function setNotification($client): void
    {
        $this->client = $client;
    }
}
