<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ClientRepository")
 */
class Client
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $fullName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string")
     * @Assert\Url(
     *    protocols = {"http", "https", "ftp"}
     * )
     */
    private $linkRepository;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank(message="File should be PDF format.")
     * @Assert\File(mimeTypes={ "application/pdf" })
     */
    private $pdfFile;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Post", inversedBy="client")
     */
    private $posts;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Notification", mappedBy="client")
     */
    private $notification;

    public function __construct()
    {
        $this->notification = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getFullName(): ?string
    {
        return $this->fullName;
    }

    /**
     * @param string $fullName
     *
     * @return Client
     */
    public function setFullName(string $fullName): self
    {
        $this->fullName = $fullName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     *
     * @return Client
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLinkRepository(): ?string
    {
        return $this->linkRepository;
    }

    /**
     * @param string $linkRepository
     *
     * @return Client
     */
    public function setLinkRepository(string $linkRepository): self
    {
        $this->linkRepository = $linkRepository;

        return $this;
    }

    public function getPdfFile()
    {
        return $this->pdfFile;
    }

    /**
     * @param $pdfFile
     */
    public function setPdfFile($pdfFile): void
    {
        $this->pdfFile = $pdfFile;
    }

    /**
     * @return mixed
     */
    public function getPosts()
    {
        return $this->posts;
    }

    /**
     * @param mixed $posts
     */
    public function setPosts($posts): void
    {
        $this->posts = $posts;
    }
}
