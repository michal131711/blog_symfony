<?php

// przestrzeń nazw dzięki temu możemy segregowac klasy wedle ich lokalizacji
namespace App\Entity;

// use umożliwia wykorzystanie danych zasobó innych klas wykorzystując alias
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

// klasa user implemetuje interfejsc który ma 5 metod do hasła do roli użytkownika i nazwy jego
// Model czyli logika apliakacji skąłda się z pół takich jakie są w bazie dzięki czemu możan pobrać wiersze z bazy
// oraz je zapisać get służa do pobierania danych a set do wykoanania zapisu do bazy
// use TimestampableEntity jest Traitem czyli kodem który można wszędzie w ten sam sposób tylko go użyć
// pole $id z komentarzami jest kluczem głównym który się inkrementuje w bazie danych
//ma swój typ integer jako liczba z kolei modyfikator private służy do ochrony tego pola dzięki niemu
// tego pola nie można wywołac po za klasą bądż w innych klasach
// protected umożliwia wykorzystanie tego pola tylko w obrębie tej kalsy i innych kals które chcą go użyć oprócz po
// za klasami publi umożliwia wykorzystanie tego pola wszedzie i w klasach i po za nią.

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="`user`")
 */
class User implements UserInterface
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", nullable=false)
     */
    private $id;
    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank(message="Pole nazwy użytkownika/username nie może być puste")
     */
    private $username;
    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Assert\NotBlank(message="Pole email nie może być puste")
     */
    private $email;
    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @Assert\Regex(pattern="/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{6,}$/",
     *     message="Hasło musi się zaczynać z dużej
     *     litery oraz musi mieć co najmniej 6 znaków oraz musi być takie samo")
     * @Assert\NotBlank(message="Hasło nie może być puste"))
     * @Assert\NotNull()
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $facebook_id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $token;

    /**
     * @var bool
     * @Assert\IsTrue(
     *     message="Proszę zatwierdzić pole związane z polityką RODO."
     * )
     * @ORM\Column(name="termsAccepted", type="boolean")
     */
    private $termsAccepted;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $gmailID;

    /**
     * @return bool
     */
    public function isTermsAccepted(): ?bool
    {
        return $this->termsAccepted;
    }

    /**
     * @param bool $termsAccepted
     *
     * @return bool
     */
    public function setTermsAccepted(bool $termsAccepted = false)
    {
        return $this->termsAccepted = $termsAccepted;
    }

    /**
     * @return int
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param int $token
     */
    public function setToken($token): void
    {
        $this->token = $token;
    }

    /**
     * @return string
     */
    public function getFacebookId()
    {
        return $this->facebook_id;
    }

    /**
     * @param string $facebook_id
     */
    public function setFacebookId($facebook_id): void
    {
        $this->facebook_id = $facebook_id;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string)$this->username;
    }

    /**
     * @param $username
     */
    public function setUsername($username): void
    {
        $this->username = $username;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    /**
     * @param array $roles
     *
     * @return User
     */
    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string)$this->password;
    }

    /**
     * @param string $password
     *
     * @return User
     */
    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return string $email
     */
    public function getGmailID(): ?string
    {
        return $this->gmailID;
    }

    /**
     * @param string $gmailID
     */
    public function setGmailID(string $gmailID): self
    {
        $this->gmailID = $gmailID;

        return $this;
    }

    /**
     * @return string $email
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     *
     * @return User $email
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function validate(array $array)
    {
    }
}
