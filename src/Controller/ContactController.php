<?php

namespace App\Controller;

use App\Form\ContactType;
use App\Services\emails\EmailContactBuissness;
use App\Services\notifications\NotificationService;
use Swift_Mailer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ContactController extends AbstractController
{
    /**
     * @Route("/contact", name="contact", options={"expose"=true}, methods="GET|POST")
     * @param Request $request
     * @param Swift_Mailer $mailer
     * @param EmailContactBuissness $emailContactBuissness
     * @param NotificationService $notificationService
     * @return Response
     */
    public function contactToAdmin(
        Request $request,
        Swift_Mailer $mailer,
        EmailContactBuissness $emailContactBuissness,
        NotificationService $notificationService
    ): Response {
        $contact = [
            'full_name' => $this->getUser() != null ?
                $this->getUser()->getUsername() :
                $request->request->get('contact')['full_name'],
            'email' => $this->getUser() != null ?
                $this->getUser()->getEmail() :
                $request->request->get('contact')['email'],
            'title' => $request->request->get('contact')['title'],
            'body' => $request->request->get('contact')['body'],
        ];
        $form = $this->createForm(ContactType::class, $contact);
        $findAllApplicationsNotifyByUser = $notificationService->findAllApplicationsNotifyByUser($this->getUser());
        $countAllApplicationsNotifyByUser = $notificationService->countAllApplicationsNotifyByUser($this->getUser());
        $form->handleRequest($request);

        if ($request->isXmlHttpRequest() &&
            $request->request->all()) {
            if ($form->isSubmitted() &&
                $form->isValid()) {
                $message = $emailContactBuissness->sendContactMessage(
                        "Ważna informacji ze strony wwww.technology-work.com",
                        $contact['email'],
                        'michal131711@gmail.com',
                        $this->render(
                            'contact/contact.html.twig',
                            [
                                'messageContact' => $contact,
                            ]
                        ),
                        'text/html'
                    );
                $mailer->send($message);
            }
        }
        return $this->render('contact/index.html.twig', [
            'findAllApplicationsNotifyByUser' => $findAllApplicationsNotifyByUser,
            'countAllApplicationsNotifyByUser' => $countAllApplicationsNotifyByUser,
            'form' => $form->createView(),
        ]);
    }
}
