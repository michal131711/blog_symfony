<?php

namespace App\Controller;

use App\Repository\PostRepository;
use App\Services\notifications\NotificationService;
use App\Services\posts\PostService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/Start")
 */
class HomeController extends AbstractController
{
    /**
     * @Route("", name="homeProfile")
     * @param PostService $postService
     * @param NotificationService $notificationService
     * @return Response
     */
    public function index(PostService $postService, NotificationService $notificationService): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $findAllApplicationsNotifyByUser = $notificationService->findAllApplicationsNotifyByUser($this->getUser());
        $countAllApplicationsNotifyByUser = $notificationService->countAllApplicationsNotifyByUser($this->getUser());

        $getPosts = $postService->findAllUserOffer($this->getUser());
        return $this->render(
            'home/index.html.twig',
            [
                'findAllApplicationsNotifyByUser' => $findAllApplicationsNotifyByUser,
                'countAllApplicationsNotifyByUser' => $countAllApplicationsNotifyByUser,
                'posts' => $getPosts
            ]
        );
    }
}
