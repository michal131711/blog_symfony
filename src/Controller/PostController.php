<?php

namespace App\Controller;

use App\Entity\Client;
use App\Entity\Notification;
use App\Entity\NotificationMessage;
use App\Entity\Post;
use App\Entity\User;
use App\Form\ClientType;
use App\Form\NotificationMessageType;
use App\Form\PostType;
use App\Services\categories\CategoryService;
use App\Services\clients\ClientService;
use App\Services\files\FileUploaderService;
use App\Services\notifications\NotificationService;
use App\Services\posts\PostService;
use App\Traits\PaginatePostTrait;
use App\Traits\SearchPostTrait;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * @Route("/post")
 */
class PostController extends Controller
{
    use SearchPostTrait;
    use PaginatePostTrait;

    /**
     * @Route("/notification/{id<\d+>}", methods="POST", name="notification_list", options={"expose"=true})
     *
     * @param $id
     * @param Request $request
     * @param NotificationMessage $notificationMessage
     * @param NotificationService $notificationService
     * @param ObjectNormalizer $objectNormalizer
     * @return Response
     */
    public function notification(
        $id,
        Request $request,
        NotificationService $notificationService
    ): ?Response
    {
        $notifyMessage = $notificationService->findNotificationMessageById($id);
        $form = $this->createForm(NotificationMessageType::class, $notifyMessage);
        $form->handleRequest($request);

        $getValCheckbox = $request->get('notification')['redOffer'];

        $request->isXmlHttpRequest();
        $form->isSubmitted();
        $notifyMessage->setName('Obejrzana');
        $notifyMessage->setRedOffer($getValCheckbox);
        $notificationService->saveNotify()->persist($notifyMessage);
        $notificationService->saveNotify()->flush();
        return $this->redirectToRoute('post_index');
    }

    /**
     * @Route("/", name="post_index", methods="GET|POST", options={"expose"=true})
     *
     * @param PostService $postService
     * @param Request $request
     * @param Notification $notification
     * @param CategoryService $categoryService
     *
     * @param NotificationService $notificationService
     * @return Response
     */
    public function index(
        PostService $postService,
        Request $request,
        CategoryService $categoryService,
        NotificationService $notificationService): Response
    {
        $category = $categoryService->getAllCategories();
        $getPosts = $postService->getAllPost();
        $countAllApplicationsNotifyByUser = $notificationService->countAllApplicationsNotifyByUser($this->getUser());
        $findAllApplicationsNotifyByUser = $notificationService->findAllApplicationsNotifyByUser($this->getUser());


        $getAllUsers = $this->getDoctrine()->getRepository(User::class)->findAll();
        $paginator = $this->get('knp_paginator');
        $posts = $this->paginatePosts($getPosts, $paginator, $request);
        $template = $this->render(
            'post/index.html.twig',
            [
                'getAllUsers' => $getAllUsers,
                'posts' => $posts,
                'countAllApplicationsNotifyByUser' => $countAllApplicationsNotifyByUser,
                'findAllApplicationsNotifyByUser' => $findAllApplicationsNotifyByUser,
                'category' => $category,
                'form' => $this->searchPosts()->createView(),
            ]
        )->getContent();

        $response = new Response($template, Response::HTTP_OK, ['Content-type' => 'text/html; charset=UTF-8']);

        return $response;
    }

    /**
     * @Route("/find/{_query?}", name="title_search", methods="POST|GET")
     *
     * @param Request $request
     * @param CategoryService $categoryService
     * @param PostService $postService
     *
     * @param NotificationService $notificationService
     * @return JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function titleSearch(
        Request $request,
        CategoryService $categoryService,
        PostService $postService,
        NotificationService $notificationService)
    {

        $category = $categoryService->getAllCategories();
        $countAllApplicationsNotifyByUser = $notificationService->countAllApplicationsNotifyByUser($this->getUser());
        $findAllApplicationsNotifyByUser = $notificationService->findAllApplicationsNotifyByUser($this->getUser());

        $_query = $request->request->get('form') ? null : $request->request->get('form');
        if ($_citySearchQuery = $request->request->get('form')) {
            $getPosts = $postService->titleSearch($_citySearchQuery['Search'] ? $_citySearchQuery['Search'] : null, $_citySearchQuery['Search_city']);
            $paginator = $this->get('knp_paginator');
            $posts = $this->paginatePosts($getPosts, $paginator, $request);

            return $this
                ->render(
                    'post/index.html.twig',
                    [
                        'posts' => $posts,
                        'findAllApplicationsNotifyByUser' => $findAllApplicationsNotifyByUser,
                        'countAllApplicationsNotifyByUser' => $countAllApplicationsNotifyByUser,
                        'category' => $category,
                        'form' => $this->searchPosts()->createView(),
                    ]
                );
        } else {
            $posts = $postService->getAllPost();
        }
        $data = $postService->resultFindPosts()->serialize($posts, 'json');
        if (null === $_query) {
            return new JsonResponse($data, 200, [], true);
        } else {
            return $this->redirectToRoute('post_index');
        }
    }

    /**
     * @Route("/subpost/{id<\d+>}", name="post_category_index", methods="GET|POST")
     *
     * @param Request $request
     * @param CategoryService $categoryService
     * @param PostService $postService
     * @param NotificationService $notificationService
     *
     * @return Response
     */
    public function postCategoryIndex(
        Request $request,
        CategoryService $categoryService,
        PostService $postService,
        NotificationService $notificationService
    ): Response
    {
        $category = $categoryService->getAllCategories();
        $post = $postService->getAllPost();
        $countAllApplicationsNotifyByUser = $notificationService->countAllApplicationsNotifyByUser($this->getUser());
        $findAllApplicationsNotifyByUser = $notificationService->findAllApplicationsNotifyByUser($this->getUser());
        $getPostsCategory = $postService->findCategoryById($request->get('id'));
        $paginator = $this->get('knp_paginator');

        if (null == $getPostsCategory) {
            return $this->render(
                'post/index.html.twig',
                [
                    'countAllApplicationsNotifyByUser' => $countAllApplicationsNotifyByUser,
                    'findAllApplicationsNotifyByUser' => $findAllApplicationsNotifyByUser,
                    'postsCategoryName' => $getPostsCategory,
                    'category' => $category,
                    'posts' => $post,
                    'form' => $this->searchPosts()->createView(),
                ]
            );
        }
        $postsCategory = $this->paginatePosts(
            $getPostsCategory[0]->getPosts(),
            $paginator,
            $request
        );

        return $this->render(
            'post/index.html.twig',
            [
                'postsCategory' => $postsCategory,
                'findAllApplicationsNotifyByUser' => $findAllApplicationsNotifyByUser,
                'countAllApplicationsNotifyByUser' => $countAllApplicationsNotifyByUser,
                'postsCategoryName' => $getPostsCategory[0],
                'category' => $category,
                'posts' => $post,
                'form' => $this->searchPosts()->createView(),
            ]
        );
    }

    /**
     * @Route("/create/post", name="post_new", methods="GET|POST", options={"expose"=true})
     *
     * @param Request $request
     * @param PostService $postService
     *
     * @return Response
     */
    public function new(Request $request, PostService $postService, NotificationService $notificationService): Response
    {
        $post = new Post();
        $form = $this->createForm(PostType::class, $post);
        $findAllApplicationsNotifyByUser = $notificationService->findAllApplicationsNotifyByUser($this->getUser());
        $countAllApplicationsNotifyByUser = $notificationService->countAllApplicationsNotifyByUser($this->getUser());

        if ($request->isXmlHttpRequest()) {
            $form->submit($request->request->all(), true);
            if ($form->isSubmitted() && $form->isValid()) {
                $post->setUser($this->getUser());
                $postService->createPost()->persist($post);
                $postService->createPost()->flush();
            } else {
                $temaplate = $this
                    ->render(
                        'post/new.html.twig',
                        [
                            'findAllApplicationsNotifyByUser' => $findAllApplicationsNotifyByUser,
                            'countAllApplicationsNotifyByUser' => $countAllApplicationsNotifyByUser,
                            'post' => $post,
                            'form' => $form->createView(),
                        ]
                    )->getContent();
                $response = new Response($temaplate, Response::HTTP_BAD_REQUEST, ['Content-type' => 'text/html; charset=UTF-8']);

                return $response;
            }
        }
        if ($this->getUser()) {
            return $this
                ->render(
                    'post/new.html.twig',
                    [
                        'findAllApplicationsNotifyByUser' => $findAllApplicationsNotifyByUser,
                        'countAllApplicationsNotifyByUser' => $countAllApplicationsNotifyByUser,
                        'post' => $post,
                        'form' => $form->createView(),
                    ]
                );
        } else {
            if (!$this->isGranted($post)) {
                return $this->redirect('https://technology-work.com/_error/403');
            }
        }
    }

    /**
     * @Route("/info/{id<\d+>}", name="post_show", options={"expose"=true}, methods="GET|POST")
     * @ParamConverter("post", class="App\Entity\Post")
     *
     * @param Request $request
     * @param $id
     * @param PostService $postService
     * @param ClientService $clientService
     * @param Client $client
     * @param ObjectManager $manager
     *
     * @return Response
     */
    public function show(
        Request $request,
        $id,
        PostService $postService,
        ClientService $clientService,
        Client $client,
        ObjectManager $manager,
        NotificationService $notificationService
    ): Response
    {
        $countAllApplicationsNotifyByUser = $notificationService->countAllApplicationsNotifyByUser($this->getUser());
        $findAllApplicationsNotifyByUser = $notificationService->findAllApplicationsNotifyByUser($this->getUser());
        $post = $postService->findPostById($id);
        $form = $this->createForm(ClientType::class, $client);
        $form->handleRequest($request);

        if ($request->isXmlHttpRequest() &&
            $request->request->all()) {
            if ($form->isSubmitted() &&
                $form->isValid()) {
                $fileUploader = new FileUploaderService($this->getParameter('files_directory'));
                $sendTo = $postService->findPostById($post->getId());

                $file = $client->getPdfFile();
                $fileName = $fileUploader->upload($file);
                $client->setPdfFile($fileName);
                $client->setPosts($sendTo);

                $notification = new Notification();

                $notification->setClient($client);
                $notification->setPost($sendTo);
                $notification->setUser($sendTo->getUser());
                $manager->persist($notification);

                $notificationMessage = new NotificationMessage();
                $notificationMessage->setNotification($notification);
                $notificationMessage->setName('Nowe powiadomienie');
                $notificationMessage->setRedOffer(false);
                $manager->persist($notificationMessage);

                $clientService->createApplicationClient(
                    $post->getTitle(),
                    $client->getEmail(),
                    null != $sendTo->getContactEmail() ?
                        $sendTo->getContactEmail() :
                        $sendTo->getUser()->getEmail(),

                    $this->render(
                        'security/applicationOfferTemplate.html.twig',
                        [
                            'form' => $form->createView(),
                            'findAllApplicationsNotifyByUser' => $findAllApplicationsNotifyByUser,
                            'countAllApplicationsNotifyByUser' => $countAllApplicationsNotifyByUser,
                            'messageClient' => $client,
                            'messagePost' => $post,
                        ]
                    ),
                    $fileName
                )->flush();
                $manager->flush();
            }
        }

        return $this->render(
            'post/show.html.twig',
            [
                'post' => $post,
                'findAllApplicationsNotifyByUser' => $findAllApplicationsNotifyByUser,
                'countAllApplicationsNotifyByUser' => $countAllApplicationsNotifyByUser,
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/{id<\d+>}/edit", name="post_edit", methods="GET|POST", options={"expose"=true})
     *
     * @param $id
     * @param Request $request
     * @param PostService $postService
     *
     * @return Response|null
     */
    public function edit($id, Request $request, PostService $postService, NotificationService $notificationService): ?Response
    {
        $post = $postService->findPostById($id);
        $form = $this->createForm(PostType::class, $post);
        $findAllApplicationsNotifyByUser = $notificationService->findAllApplicationsNotifyByUser($this->getUser());
        $countAllApplicationsNotifyByUser = $notificationService->countAllApplicationsNotifyByUser($this->getUser());

        if ($request->isXmlHttpRequest()
        ) {
            $form->submit(
                $request->request->all(),
                true
            );
            if ($form->isSubmitted() &&
                $form->isValid()) {
                $post->setUser($this->getUser());
                $postService->updatePost()->persist($post);
                $postService->updatePost()->flush();
            }
        }
        if ($post->getUser() == $this->getUser()) {
            return $this
                ->render(
                    'post/edit.html.twig',
                    [
                        'post' => $post,
                        'findAllApplicationsNotifyByUser' => $findAllApplicationsNotifyByUser,
                        'countAllApplicationsNotifyByUser' => $countAllApplicationsNotifyByUser,
                        'form' => $form->createView(),
                    ]
                );
        } else {
            if (!$this->isGranted($post)) {
                return $this->redirect('https://technology-work.com/_error/403');
            }
        }
    }

    /**
     * @Route("/{id<\d+>}", name="post_delete", methods="DELETE")
     *
     * @param Request $request
     * @param $id
     * @param PostService $postService
     *
     * @return Response
     */
    public function delete(Request $request, $id, PostService $postService): Response
    {
        if ($this->isCsrfTokenValid('delete' . $id, $request->request->get('_token'))) {
            $post = $postService->findPostById((int)$id);
            $postService->removePost($post)->flush();

            return $this->redirectToRoute('post_index');
        }
    }
}
