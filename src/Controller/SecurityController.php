<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\AccountType;
use App\Form\LoginType;
use App\Form\NewPasswordType;
use App\Form\RegisterType;
use App\Form\ResetPasswordType;
use App\Repository\UserRepository;
use App\Services\emails\EmailResetPassword;
use App\Services\notifications\NotificationService;
use App\Services\users\UserService;
use Doctrine\Common\Persistence\ObjectManager;
use Sonata\NotificationBundle\Consumer\ConsumerEvent;
use Sonata\NotificationBundle\Exception\InvalidParameterException;
use Swift_Mailer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

// kontroler odpowiada za działąnie naszych danych możemy w jego metodach wyświetlić listę np. wszystkich użytkowników
// bądż zarejestrować nowego usera do bazy. W kontrolerze odpowiada tylko za kontrolę apki jak ukazanie widoku
// html np. z danymi formularz operacje zapisu, usunięcie i edycji
// exteds służy do dziedziczenia innej klasy na rzecz tej której użytkujemy
// przekazujemy meteody z parenta do childa aby zachować porzadek w kodzie i nie powielać tych danych z jego operacjami
// Abstract kontroler jest klasa abstrakcyjną czyli taką której nie można po za klasa wywołać jego instancji
// Klasa abstrakcyjna służy przeważnie jako klasa bazowa dla tej której chce ja wykorzystać.
class SecurityController extends AbstractController
{
    /**
     * @Route("/register", name="register", options={"expose"=true}, methods={"GET|POST"})
     *
     * @param Request $request
     * @param ObjectManager $objectManager
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param ValidatorInterface $validator
     *
     * @return RedirectResponse|Response
     */
    public function registration(
        Request $request,
        ObjectManager $objectManager,
        UserPasswordEncoderInterface $passwordEncoder,
        ValidatorInterface $validator,
        NotificationService $notificationService
    ) {
        $user = new User();
        $form = $this->createForm(
            RegisterType::class,
            $user
        );
        $form->handleRequest($request);
        $findAllApplicationsNotifyByUser = $notificationService->findAllApplicationsNotifyByUser($this->getUser());
        $countAllApplicationsNotifyByUser = $notificationService->countAllApplicationsNotifyByUser($this->getUser());
        $errors = $validator->validate($user);

        if ($request->isXmlHttpRequest() &&
            $request->request->all()) {
            if ($form->isValid() &&
                $form->isSubmitted()) {
                $hash = $passwordEncoder->encodePassword($user, $user->getPassword());

                $user->setPassword($hash);
                $user->setRoles(
                    [
                        'roles' => 'ROLE_USER',
                    ]
                );
                $user->setToken($request->getSession()->get('_csrf/https-register'));
                $user->setTermsAccepted($request->request->get('register')['termsAccepted']);
                $objectManager->persist($user);
                $objectManager->flush();

                return $this->render(
                    'security/register.html.twig',
                    [
                        'form' => $form
                            ->createView(),
                        'findAllApplicationsNotifyByUser' => $findAllApplicationsNotifyByUser,
                        'countAllApplicationsNotifyByUser' => $countAllApplicationsNotifyByUser,
                        'error' => $errors,
                    ]
                );
            } else {
                $temaplate = $this
                    ->render(
                        'security/register.html.twig',
                        [
                            'form' => $form
                                ->createView(),
                            'findAllApplicationsNotifyByUser' => $findAllApplicationsNotifyByUser,
                            'countAllApplicationsNotifyByUser' => $countAllApplicationsNotifyByUser,
                            'error' => $errors,
                        ]
                    )->getContent();
                $response = new Response($temaplate, Response::HTTP_BAD_REQUEST, ['Content-type' => 'text/html; charset=UTF-8']);

                return $response;
            }
        }

        return $this->render(
            'security/register.html.twig',
            [
                'findAllApplicationsNotifyByUser' => $findAllApplicationsNotifyByUser,
                'countAllApplicationsNotifyByUser' => $countAllApplicationsNotifyByUser,
                'form' => $form
                    ->createView(),
            ]
        );
    }

    /**
     * @Route("/login", name="login", methods={"GET|POST"})
     */
    public function login(NotificationService $notificationService): Response
    {
        $form = $this
            ->createForm(
                LoginType::class,
                null,
                [
                    'action' => $this->generateUrl('login'),
                ]
            );
        $findAllApplicationsNotifyByUser = $notificationService->findAllApplicationsNotifyByUser($this->getUser());
        $countAllApplicationsNotifyByUser = $notificationService->countAllApplicationsNotifyByUser($this->getUser());

        return $this->render(
            'security/login.html.twig',
            [
                'findAllApplicationsNotifyByUser' => $findAllApplicationsNotifyByUser,
                'countAllApplicationsNotifyByUser' => $countAllApplicationsNotifyByUser,
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logout(): Response
    {
        return $this->render('post/index.html.twig');
    }

    /**
     * @Route("/profile", name="profile")
     */
    public function profile(): Response
    {
        return $this->render('security/profile.html.twig');
    }

    /**
     * @Route("/resetPassword", name="resetPassword")
     * @param Request $request
     * @param UserService $userService
     * @param Swift_Mailer $mailer
     * @param EmailResetPassword $emailResetPassword
     * @return RedirectResponse|Response
     */
    public function resetPassword(
        Request $request,
        UserService $userService,
        Swift_Mailer $mailer,
        EmailResetPassword $emailResetPassword,
        NotificationService $notificationService
    ) {
        $userInfo = ['email' => null];
        $form = $this->createForm(ResetPasswordType::class, $userInfo);
        $findAllApplicationsNotifyByUser = $notificationService->findAllApplicationsNotifyByUser($this->getUser());
        $countAllApplicationsNotifyByUser = $notificationService->countAllApplicationsNotifyByUser($this->getUser());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $tokenreset = $userService->findUserByName(
                [
                    'email' => $request->get('reset_password')['email'],
                ]
            );
            $message = $emailResetPassword->sendResetEmail(
                'Resetowanie hasła',
                'michal131711@gmail.com',
                $request->get('reset_password')['email'],
                $this->render(
                    'security/mail.html.twig',
                    [
                        'form' => $form->createView(),
                        'countAllApplicationsNotifyByUser' => $countAllApplicationsNotifyByUser,
                        'tokenreset' => $tokenreset,
                    ]
                ),
                'text/html'
            );
            $mailer->send($message);
        }

        // Wszędzie jest render ze ściężką do pliku html oraz jakieś dane do niej przypisane
        // oznacza to że te dane możemy wywołac na pliku html który wyświetli dane z tych zmienny
        // jak findAllApplicationsNotifyByUser czyli znajlezienie wszystkich powiadomień tego usera
        // z count to liczbowo ile jest powiadomień dla danego usera
        // i sam formularz
        return $this->render('security/resetPassword.html.twig', [
            'findAllApplicationsNotifyByUser' => $findAllApplicationsNotifyByUser,
            'countAllApplicationsNotifyByUser' => $countAllApplicationsNotifyByUser,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{token}/changePassword", name="changePassword", methods={"GET|POST"})
     *
     * @param Request $request
     * @param UserRepository $userRepository
     * @param $token
     * @param UserPasswordEncoderInterface $encoder
     * @param ObjectManager $objectManager
     *
     * @return RedirectResponse|Response
     */
    public function changePassword(
        Request $request,
        $token,
        UserService $userService,
        UserPasswordEncoderInterface $encoder,
        ObjectManager $objectManager,
        NotificationService $notificationService
    ) {
        $userInfo = ['token' => $token, 'password' => ''];
        $form = $this->createForm(NewPasswordType::class, $userInfo);
        $countAllApplicationsNotifyByUser = $notificationService->countAllApplicationsNotifyByUser($this->getUser());
        $findAllApplicationsNotifyByUser = $notificationService->findAllApplicationsNotifyByUser($this->getUser());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $userInfo = $form->getData();
            $user = $userService->findUserByName(['token' => $token])[0];
            $plainPassword = $userInfo['password'];
            $password = $encoder->encodePassword($user, $plainPassword);
            $user->setPassword($password);
            $objectManager->persist($user);
            $objectManager->flush();

            return $this->redirectToRoute('login');
        }

        return $this->render(
            'security/newpassword.html.twig',
            [
                'findAllApplicationsNotifyByUser' => $findAllApplicationsNotifyByUser,
                'countAllApplicationsNotifyByUser' => $countAllApplicationsNotifyByUser,
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/account", name="account", methods={"GET|POST"})
     *
     * @param Request $request
     * @param UserRepository $userRepository
     * @param UserPasswordEncoderInterface $encoder
     * @param ObjectManager $objectManager
     *
     * @return RedirectResponse|Response
     */
    public function account(
        Request $request,
        UserRepository $userRepository,
        UserPasswordEncoderInterface $encoder,
        ObjectManager $objectManager,
        NotificationService $notificationService
    ) {
        $userInfo = ['username' => '', 'email' => $this->getUser()->getEmail(), 'password'];

        $form = $this->createForm(AccountType::class, $userInfo);
        $form->handleRequest($request);
        $findAllApplicationsNotifyByUser = $notificationService->findAllApplicationsNotifyByUser($this->getUser());
        $countAllApplicationsNotifyByUser = $notificationService->countAllApplicationsNotifyByUser($this->getUser());

        if ($form->isSubmitted() && $form->isValid()) {
            $userInfo = $form->getData();
            $username = $userInfo['username'];
            $email = $userInfo['email'];
            $plainPassword = $userInfo['password'];
            $user = $userRepository->findOneBy(['email' => $email]);
            if (null === $user) {
                $this->addFlash('danger', 'Invalid email');

                return $this->redirectToRoute('resetPassword');
            }
            $user->setUsername($username);
            $password = $encoder->encodePassword($user, $plainPassword);
            $user->setPassword($password);
            $objectManager->persist($user);
            $objectManager->flush();

            return $this->redirectToRoute('login');
        }

        return $this->render('security/account.html.twig', [
            'findAllApplicationsNotifyByUser' => $findAllApplicationsNotifyByUser,
            'countAllApplicationsNotifyByUser' => $countAllApplicationsNotifyByUser,
            'form' => $form->createView(),
        ]);
    }
}