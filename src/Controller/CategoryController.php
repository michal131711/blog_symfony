<?php

namespace App\Controller;

use App\Entity\Category;
use App\Form\CategoryType;
use App\Services\categories\CategoryService;
use App\Services\users\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/category")
 */
class CategoryController extends AbstractController
{
    /**
     * @Route("/", name="category_index", methods="GET")
     * @param CategoryService $categoryService
     * @param UserService $userService
     * @return Response
     */
    public function index(CategoryService $categoryService, UserService $userService): Response
    {
        $user = $userService->findUserByName(['username' => 'Administrator']);
        $getAllCategory = $categoryService->getAllCategories();

        if ($this->isGranted('ROLE_ADMIN') == $userService->findUserByName(['roles' => 'ROLE_ADMIN'])) {
            return $this->redirect('https://technology-work.com/_error/403');
        }
        return $this->render('category/index.html.twig', [
            'categories' => $getAllCategory,
            'user' => $user,
        ]);
    }

    /**
     * @Route("/new", name="category_new", methods="GET|POST")
     * @param Request $request
     * @return Response
     */
    public function new(Request $request, CategoryService $categoryService, UserService $userService): Response
    {
        $user = $userService->findUserByName(['username' => 'Administrator']);
        if ($this->isGranted('ROLE_ADMIN') == $userService->findUserByName(['roles' => 'ROLE_ADMIN'])) {
            return $this->redirect('https://technology-work.com/_error/403');
        }
        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $categoryService->createCategory($category)->flush();

            return $this->redirectToRoute('category_index');
        }

        return $this->render('category/new.html.twig', [
            'category' => $category,
            'form' => $form->createView(),
            'user' => $user,
        ]);
    }

    /**
     * @Route("/{id<\d+>}", name="category_show", methods="GET")
     * @param $id
     * @param CategoryService $categoryService
     * @param UserService $userService
     * @return Response
     */
    public function show($id, CategoryService $categoryService, UserService $userService): Response
    {
        $user = $userService->findUserByName(['username' => 'Administrator']);
        $category = $categoryService->findCategoryById($id);
        if ($this->isGranted('ROLE_ADMIN') == $userService->findUserByName(['roles' => 'ROLE_ADMIN'])) {
            return $this->redirect('https://technology-work.com/_error/403');
        }
        return $this->render(
            'category/show.html.twig',
            [
                'category' => $category,
                'user' => $user
            ]
        );
    }

    /**
     * @Route("/{id<\d+>}/edit", name="category_edit", methods="GET|POST")
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function edit(Request $request, $id, CategoryService $categoryService, UserService $userService): Response
    {
        $category = $categoryService->findCategoryById($id);
        $user = $userService->findUserByName(['username' => 'Administrator']);
        if ($this->isGranted('ROLE_ADMIN') == $userService->findUserByName(['roles' => 'ROLE_ADMIN'])) {
            return $this->redirect('https://technology-work.com/_error/403');
        }
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $categoryService->updateCategory()->flush();
            return $this->redirectToRoute('category_index');
        }

        return $this->render('category/edit.html.twig', [
            'category' => $category,
            'form' => $form->createView(),
            'user' => $user,
        ]);
    }

    /**
     * @Route("/{id<\d+>}", name="category_delete", methods="DELETE")
     * @param CategoryService $categoryService
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function delete(CategoryService $categoryService, Request $request, $id): Response
    {
        if ($this->isCsrfTokenValid('delete' . $id, $request->request->get('_token'))) {
            $category = $categoryService->findCategoryById($id);
            $categoryService->removeCategory($category)->flush();
        }
        return $this->redirectToRoute('category_index');
    }
}
