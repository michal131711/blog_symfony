<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\City;
use App\Entity\Country;
use App\Entity\Post;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PostType extends AbstractType
{
    private $managerRegistry;

    public function __construct(EntityManagerInterface $managerRegistry)
    {
        $this->managerRegistry = $managerRegistry;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'Tytuł oferty',
                'required' => false,
            ])
            ->add('nameSpecializations', TextType::class, [
                'label' => 'Specjalizacja',
                'required' => false,
            ])
            ->add('companyName', TextType::class, [
                'label' => 'Nazwa firmy',
                'required' => false,
            ])
            ->add('country', EntityType::class, [
                'class' => Country::class,
                'label' => 'Kraj',
                'placeholder' => 'Wybierz kraj',
                'required' => false,
                'choice_label' => function ($country) {
                    return $country->getName();
                },
            ])
            ->add('city', TextType::class, [
                'label' => 'Miejscowość',
                'required' => false,
            ])
            ->add('address', TextType::class, [
                'label' => 'Adres firmy',
                'required' => false,
            ])
            ->add('numberBuild', TextType::class, [
                'label' => 'Numer budynku',
                'required' => false,
            ])
            ->add('numberHome', TextType::class, [
                'label' => 'Numer mieszkania lub apartamentu',
                'required' => false,
            ])
            ->add('postCode', TextType::class, [
                'label' => 'Kod pocztowy firmy',
                'required' => false,
            ])
            ->add('body', TextareaType::class, [
                'label' => 'Opis oferty',
                'required' => false,
            ])
            ->add('category', EntityType::class, [
                'class' => Category::class,
                'label' => 'Kategorie oferty',
                'attr' => ['class' => 'mutli-select'],
                'required' => false,
                'multiple' => true,
                'choice_label' => function ($category) {
                    return $category->getName();
                },
            ])
            ->add('priceOffer', MoneyType::class, [
                'label' => 'Widełki',
                'required' => false,
            ])
            ->add('currency', TextType::class, [
                'label' => 'Nazwa waluty',
                'required' => false,
            ])
            ->add('contactPhone', TextType::class, [
                'label' => 'Telefon kontaktowy',
                'required' => false,
            ])
            ->add('contactEmail', EmailType::class, [
                'label' => 'Kontakt Email',
                'attr' => [
                    'id' => 'form_application_client_send',
                ],
                'required' => false,
                'help' => 'To pole jest opcjonalne nie musisz podawać drugi raz maila',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Post::class,
        ]);
    }

    /**
     * This will remove formTypeName from the form.
     */
    public function getBlockPrefix()
    {
        return null;
    }
}
