<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;

class RegisterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'username',
                TextType::class,
                [
                    'required'  => false,
                ]
            )
            ->add(
                'email',
                EmailType::class,
                [
                    'required'  =>  false,
                ]
            )
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'options' => array('attr' => array('class' => 'password-field')),
                'required' => false,
                'first_options' => array('label' => 'Password'),
                'second_options' => array('label' => 'Repeat Password'),])
            ->add('termsAccepted', CheckboxType::class, array(
                'label' => 'Niniejszym wyrażam zgodę na przetwarzanie moich danych osobowych zawartych w formularzu 
                w celach marketingowych serwisu technology-work.com, zgodnie z ustawą z dn. 29 sierpnia 1999 r. 
                o ochronie danych osobowych. (Dz. U. Nr 133, poz. 883).',
                'required' => false,
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        if (!$resolver) {
            $resolver->setDefaults([
                'data_class' => User::class,
                'csrf_token_id' => true
            ]);
        } else {
            return null;
        }
    }
}
