<?php

namespace App\Form;

use App\Entity\Notification;
use App\Entity\NotificationMessage;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NotificationMessageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('redOffer', CheckboxType::class, [
                'required' => false
            ]);
//            ->add('notification', EntityType::class, [
//                'class' => Notification::class,
//                'required' => false,
//                'multiple' => true,
//                'choice_label' => function ($notification) {
//                    return $notification;
//                },
//            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        if (!$resolver) {
            $resolver->setDefaults([
                'csrf_protection' => false,
                'data_class' => NotificationMessage::class,
            ]);
        } else {
            return null;
        }
    }

    /**
     * This will remove formTypeName from the form.
     */
    public function getBlockPrefix()
    {
        return null;
    }
}
