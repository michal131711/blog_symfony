<?php namespace App\Tests;

class UserCest
{
    // tests
    public function tryToTest(AcceptanceTester $I)
    {
        $I->amOnPage('/');
        $I->see('Oferty pracy dla młodych programistów', 'title');
    }

    public function testTryToLogin(AcceptanceTester $I)
    {
        $I->amOnPage('/login');
        $I->fillField('Email', 'Miles');
        $I->fillField('Password', 'Davis');
        $I->click('Zaloguj się');
        $I->see('Oferty praktyk/Stażu', 'h1');
    }

    public function tryToRegister(AcceptanceTester $I)
    {
        $I->amOnPage('/register');
        $I->submitForm('#register_user', [
            '#username'  =>  'Rowan Etkins',
            '#email'  =>  'grg@gr.feghr',
            '#register_password_first'  =>  'Daviskertaniz26',
            '#register_password_second'  =>  'Daviskertaniz26',
            '#register_termsAccepted'  =>  true,
        ], 'Zarejestruj się');
        $I->sendAjaxPostRequest('/register');
        $I->seeResponseCodeIs(200);
    }
}
