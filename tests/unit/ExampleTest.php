<?php

namespace App\Tests;

use App\Entity\User;

class ExampleTest extends \Codeception\Test\Unit
{
    /**
     * @var \App\Tests\UnitTester
     */
    protected $tester;
    
    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testValidationUser()
    {
        $user = new User();

        $this->assertFalse($user->setUsername(null));

        $user->setUsername('davert');
        $this->assertTrue($user->validate(['username']));
    }
}
