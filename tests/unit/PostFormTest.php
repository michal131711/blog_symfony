<?php
/**
 * Created by IntelliJ IDEA.
 * User: micha
 * Date: 30.12.18
 * Time: 12:54
 */

namespace App\Tests\unit;

use App\Entity\Post;
use App\Form\PostType;
use App\Tests\FunctionalTester;
use Doctrine\Common\Collections\ArrayCollection;
use Faker\Factory;
use Symfony\Component\Form\Test\TypeTestCase;

class PostFormTest
{
    public function tryToTest(FunctionalTester $I)
    {
        $faker = Factory::create();
        $formData = [
            'title' => $faker->jobTitle,
            'companyName' => $faker->company,
            'nameSpecializations' => $faker->title,
            'priceOffer' => 453.0,
            'country' => $faker->country,
            'city' => $faker->city,
            'address' => $faker->address,
            'numberBuild' => $faker->streetAddress,
            'numberHome' => '34C',
            'category' => $faker->name,
            'postCode' => $faker->postcode,
            'body' => $faker->text,
            'currency' => $faker->currencyCode,
            'contactPhone' => $faker->phoneNumber,
            'contactEmail' => $faker->companyEmail,
        ];
        $I->amOnPage('/post/create/post');
        $I->fillField('#title', $formData['title']);
        $I->fillField('#companyName', $formData['companyName']);
        $I->fillField('#nameSpecializations', $formData['nameSpecializations']);
        $I->fillField('#priceOffer', $formData['priceOffer']);
        $I->fillField('#country', $formData['country']);
        $I->fillField('#city', $formData['city']);
        $I->fillField('#address', $formData['address']);
        $I->fillField('#numberBuild', $formData['numberBuild']);
        $I->fillField('#numberHome', $formData['numberHome']);
        $I->fillField('#category', $formData['category']);
        $I->fillField('#postCode', $formData['postCode']);
        $I->fillField('#body', $formData['body']);
        $I->fillField('#currency', $formData['currency']);
        $I->fillField('#contactPhone', $formData['contactPhone']);
        $I->fillField('#contactEmail', $formData['contactEmail']);
        $I->click('Dodaj');
    }
}
