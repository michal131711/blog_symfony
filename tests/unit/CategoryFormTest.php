<?php
/**
 * Created by IntelliJ IDEA.
 * User: micha
 * Date: 30.12.18
 * Time: 12:54
 */

namespace App\Tests\unit;

use App\Entity\Category;
use App\Form\CategoryType;
use Faker\Factory;
use Symfony\Component\Form\Test\TypeTestCase;

class CategoryFormTest extends TypeTestCase
{
    /**
     * @test
     */
    public function CreateCategoryTest(): void
    {
        $faker = Factory::create();
        $formData = [
            'name' => $faker->jobTitle,
        ];
        for ($i = 0; $i <= 20; $i++) {
            $categoryToCompare = new Category();
            $form = $this->factory->create(CategoryType::class, $categoryToCompare);
            $category = new Category();
            $category->setName($formData['name']);

            $form->submit($formData);

            $this->assertTrue($form->isSynchronized());

            $this->assertEquals($category, $categoryToCompare);
        }
    }
}
