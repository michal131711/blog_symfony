<?php /** @noinspection ALL */

/**
 * Created by IntelliJ IDEA.
 * User: micha
 * Date: 27.12.18
 * Time: 17:28
 */

namespace App\Tests\unit;

use App\Entity\User;
use App\Form\RegisterType;
use Faker\Factory;
use phpDocumentor\Reflection\Types\Boolean;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

class UserControllerTest extends TestCase
{
    /**
     * @test
     */
    public function UserInstanceTest()
    {
        $user = new User();
        $this->assertInstanceOf(User::class, $user);
    }
    /**
     * @test
     */
    public function UserNotNullTest()
    {
        $user = new User();
        $this->assertNotNull($user);
    }
}
