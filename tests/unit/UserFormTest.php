<?php
/**
 * Created by IntelliJ IDEA.
 * User: micha
 * Date: 30.12.18
 * Time: 12:54
 */

namespace App\Tests\unit;

use App\Entity\User;
use App\Form\AccountType;
use App\Form\RegisterType;
use Codeception\Test\Unit;
use Faker\Factory;

class UserFormTest extends Unit
{
    /**
     * @specify
     */
    private $user;

    /**
     * @test
     */
    public function createUserTest()
    {
        $faker = Factory::create();
        $formData = [
            'username' => $faker->name,
            'email' => $faker->email,
            'password' => $faker->password,
            'termsAccepted' => true,
        ];
        for ($i = 0; $i <= 20; $i++) {
            $userToCompare = new User();
            $form = $this->factory->create(RegisterType::class, $userToCompare);

            $user = new User();
            $user->setUsername($formData['username']);
            $user->setEmail($formData['email']);
            $user->setTermsAccepted($formData['termsAccepted']);

            $form->submit($formData);

            $this->assertTrue($form->isSynchronized());
            $this->assertTrue($formData['termsAccepted']);

            $this->assertEquals($user, $userToCompare);
        }
    }

    /**
     * @test
     */
    public function validateUserRegisterTest()
    {
        $faker = Factory::create();
        $formData = [
            'username' => null,
            'email' => $faker->city,
            'password' => 'g',
            'termsAccepted' => false,
        ];
        for ($i = 0; $i <= 20; $i++) {
            $userToCompare = new User();
            $form = $this->factory->create(AccountType::class, $userToCompare);

            $user = new User();
            $user->setUsername($formData['username']);
            $user->setEmail($formData['email']);
            $user->setPassword($formData['password']);
            $this->assertFalse($formData['termsAccepted']);

            $form->submit($formData);
            $this->assertTrue($form->isSynchronized());
            $this->assertEmpty($userToCompare->getUsername());
            $this->assertEquals($form->isValid(), $userToCompare->getEmail());
            $this->assertFalse($form->isValid(), $userToCompare->getPassword());


            $this->specify("username is required", function () {
                $user = null;
                $this->assertFalse(user(['username']));
            });

            $this->specify("username is too long", function () {
                $this->user->username = 'toolooooongnaaaaaaameeee';
                $this->assertFalse($this->user->validate(['username']));
            });

            $this->specify("username is ok", function () {
                $this->user->username = 'davert';
                $this->assertTrue($this->user->validate(['username']));
            });
        }
    }
}
