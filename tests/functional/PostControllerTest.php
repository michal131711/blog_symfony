<?php

namespace App\Tests\functional;

use App\Tests\FunctionalTester;
use Faker\Factory;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PostControllerTest extends WebTestCase
{
    public function testCreatePost(FunctionalTester $I)
    {
        $faker = Factory::create();
        $formData = [
            'title' => $faker->jobTitle,
            'companyName' => $faker->company,
            'nameSpecializations' => $faker->title,
            'priceOffer' => 453.0,
            'country' => $faker->country,
            'city' => $faker->city,
            'address' => $faker->address,
            'numberBuild' => $faker->streetAddress,
            'numberHome' => '34C',
            'category' => $faker->name,
            'postCode' => $faker->postcode,
            'body' => $faker->text,
            'currency' => $faker->currencyCode,
            'contactPhone' => $faker->phoneNumber,
            'contactEmail' => $faker->companyEmail,
        ];
        $I->sendAjaxRequest('POST', '/post/create/post', $formData);
    }
}
