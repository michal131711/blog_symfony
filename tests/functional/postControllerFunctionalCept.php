<?php

use App\Tests\FunctionalTester;
use Faker\Factory;

$test = new FunctionalTester($scenario);


$test->amOnPage('/post/create/post');
$faker = Factory::create();
$formData = [
    'title' => $faker->jobTitle,
    'nameSpecializations' => $faker->title,
    'companyName' => $faker->company,
    'country' => $faker->country,
    'city' => $faker->city,
    'address' => $faker->address,
    'numberBuild' => $faker->streetAddress,
    'numberHome' => '34C',
    'postCode' => $faker->postcode,
    'body' => $faker->text,
    'category[]' => 4,
    'priceOffer' => 453,
    'currency' => $faker->currencyCode,
    'contactPhone' => $faker->phoneNumber,
    'contactEmail' => $faker->companyEmail,
];
$test->flushToDatabase();
$test->sendAjaxRequest('POST', '/post/create/post', $formData);
$test->seeResponseCodeIs(400);
