"use strict";
import routes from "../../public/js/fos_js_routes.json";
import Routing from '../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js';

import $ from "jquery";

import toastr from "../../node_modules/toastr/toastr";

Routing.setRoutingData(routes);
toastr.options = {
    "closeButton": true,
    "debug": true,
    "newestOnTop": true,
    "progressBar": true,
    "positionClass": "toast-top-center",
    "preventDuplicates": true,
    "showDuration": "68568",
    "hideDuration": "1000",
    "timeOut": 0,
    "extendedTimeOut": 0,
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut",
    "tapToDismiss": false
};

$(function () {
    let postId = document.getElementById('editPost').dataset.postId;

    $('#form_selector').submit(function (e) {

        const $form = $(e.currentTarget);
        const urlPost = Routing.generate('post_edit', {id: postId}, true);
        console.log(urlPost);
        $.ajax({
            url: urlPost,
            withCredentials: true,
            method: 'POST',
            data: $form.serialize(),
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            },

            success: function (res) {
                toastr["success"]("Zaktualizowano ofertę pracy!<br /><br /><a type='button' class='btn btn-info' href='/post'>Yes</a> <br><br>Liczymy na wasze zadowolenie donejta możecie dać <a href='https://tipanddonation.com/Technology_Work_PL' class='btn btn-danger'>Napiwek</a>");
                // $(location).attr('href', 'https://technology-work.com');

            },
            error: function (res) {

                toastr.danger("Zresetuj przęglądarkę bądż skontaktuj się z administracją", "Coś poszło nie tak!");
            }
        });
        e.preventDefault();
    });
    $('#country').change(function (e) {
        $('label[for="city"] , #city').show();
        e.preventDefault();
    });
});
