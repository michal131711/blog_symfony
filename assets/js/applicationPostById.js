"use strict";
import routes from "../../public/js/fos_js_routes.json";
import Routing from '../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js';

import $ from "jquery";

import toastr from "../../node_modules/toastr/toastr";

Routing.setRoutingData(routes);
toastr.options = {
    "closeButton": true,
    "debug": true,
    "newestOnTop": true,
    "progressBar": true,
    "positionClass": "toast-top-center",
    "preventDuplicates": true,
    "showDuration": "68568",
    "hideDuration": "1000",
    "timeOut": 0,
    "extendedTimeOut": 0,
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut",
    "tapToDismiss": false
};


$(function () {
    let postId = document.getElementById('postId').dataset.postId;

    $('#form_application_client').submit(function (e) {
        const urlPost = Routing.generate('post_show', {id: postId}, true);
        const fullName = $('#client_fullName').val();
        const email = $('#client_email').val();
        const linkRepository = $('#client_linkRepository').val();
        const file = $('#client_pdfFile')[0].files[0];
        const formData = new FormData();
        console.log(file);

        formData.append($('#client_fullName').attr('name'), fullName);
        formData.append($('#client_email').attr('name'), email);
        formData.append($('#client_linkRepository').attr('name'), linkRepository);
        formData.append('client[pdfFile]', file);
        console.log(formData);
        $.ajax({
            url: urlPost,
            data: formData,
            type: 'POST',
            contentType: false,
            processData: false,
            cache: false,
            dataType: "json",
            success: function (data, status, req) {
                toastr["success"]("Powodzenia<br /><br /><a type='button' class='btn btn-info' href='/post'>Yes</a> Możesz podziękować twórcy tej strony dając mu donejtaMożesz podziękować twórcy tej strony dając mu  <a href='https://tipanddonation.com/Technology_Work_PL' class='btn btn-danger'>donejta</a>");
            },
            error: function (err) {
                console.log(err);
                toastr['danger']("Zresetuj przęglądarkę bądż skontaktuj się z administracją", "Coś poszło nie tak!");
            }
        });
        e.preventDefault();
    });
});