"use strict";
import {OpenStreetMapProvider} from 'leaflet-geosearch';

const provider = new OpenStreetMapProvider();

provider.search({query: $("#postCode").text() + ", " + $("#city_1").text() + ", " + $("#address").text()+", "+$('#numberBuild').text()}).then(function (result) {
    console.log(result);
    var map = L.map('map').setView([result[0].y, result[0].x], 19);
    var tiles = L.esri.basemapLayer("Streets").addTo(map);
    L.marker([result[0].y, result[0].x]).addTo(map).bindPopup(result[0].label).locate({setView: true});

});

