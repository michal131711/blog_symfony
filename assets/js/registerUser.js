"use strict";
import routes from "../../public/js/fos_js_routes.json";
import Routing from '../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js';

import $ from "jquery";

import toastr from "../../node_modules/toastr/toastr";

Routing.setRoutingData(routes);
toastr.options = {
    "closeButton": true,
    "debug": true,
    "newestOnTop": true,
    "progressBar": true,
    "positionClass": "toast-top-center",
    "preventDuplicates": true,
    "showDuration": "68568",
    "hideDuration": "1000",
    "timeOut": 0,
    "extendedTimeOut": 0,
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut",
    "tapToDismiss": false
};

require('../css/app.css');
require('../js/typeahead.js');
require('jquery-validation');
$(function () {
    $('#register_user').submit(function (e) {
        const $form = $(e.currentTarget);
        const urlPost = Routing.generate('register', [], true);
        $.ajax({
            url: urlPost,
            method: 'POST',
            withCredentials: true,
            data: $form.serialize(),
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            },
            success: function (res) {
                    toastr["success"]("Dziękuję za zainteresowanie moją aplikacją<br /><br />" +
                        "<a type='button' class='btn btn-info' href='/login'>Zaloguj się</a> " +
                        "Jesli chcesz możesz wsprzeć naszą aplikację wysyłając " +
                        "<a href='https://tipanddonation.com/Technology_Work_PL' class='btn btn-danger'>donejta</a>");

            },
            error: function (xhr, ajaxOptions, thrownError) {
                for (let i = 0; i < $(xhr.responseText).find('span.form-error-message').length; i++) {
                    const errors = $(xhr.responseText).find('span.form-error-message')[i].innerText.trim();
                    const node = document.createElement('li');
                    const spanClass = $(node).addClass('list-group-item list-group-item-danger text-uppercase');
                    const textnode = document.createTextNode(errors);
                    node.appendChild(textnode);
                    $('#errorsInput').append(spanClass);
                }
            }
        });
        e.preventDefault();
    });
});