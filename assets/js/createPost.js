"use strict";
import routes from "../../public/js/fos_js_routes.json";
import Routing from '../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js';

import $ from "jquery";

import toastr from "../../node_modules/toastr/toastr";

Routing.setRoutingData(routes);
toastr.options = {
    "closeButton": true,
    "debug": true,
    "newestOnTop": true,
    "progressBar": true,
    "positionClass": "toast-top-center",
    "preventDuplicates": true,
    "showDuration": "68568",
    "hideDuration": "1000",
    "timeOut": 0,
    "extendedTimeOut": 0,
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut",
    "tapToDismiss": false
};

//$('#country').val();

$(function () {
    $('#form_selector').submit(function (e) {
        const $form = $(e.currentTarget);
        const urlPost = Routing.generate('post_new', [], true);
        $.ajax({
            url: urlPost,
            withCredentials: true,
            method: 'POST',
            data: $form.serialize(),
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            },
            success: function (res) {
                toastr["success"]("Dodano nową ofertę pracy!<br /><br />" +
                    "<a type='button' class='btn btn-info' href='/post'>Yes</a> " +
                    "Możesz podziękować twórcy tej strony dając mu  " +
                    "<a href='https://tipanddonation.com/Technology_Work_PL' class='btn btn-danger'>donejta</a>");
            },
            error: function (jqXHR) {
                for (let i = 0; i < $(jqXHR.responseText).find('span.form-error-message').length; i++) {
                    const errors = $(jqXHR.responseText).find('span.form-error-message')[i].innerText.trim();
                    const node = document.createElement('li');
                    const textnode = document.createTextNode(errors);
                    const spanClass = $(node).addClass('list-group-item list-group-item-danger text-uppercase');
                    node.appendChild(textnode);
                    $('#errorsInput').prepend(spanClass);
                }
            }
        });

        e.preventDefault();
    });

    $('#country').change(function (e) {
        $('label[for="city"] , #city').show();
        e.preventDefault();
    });

});
