/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */
"use strict";
import toastr from "toastr";

import routes from "../../public/js/fos_js_routes.json";
import Routing from '../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js';

import $ from "jquery";

require('../css/app.css');
require('../js/typeahead.js');
Routing.setRoutingData(routes);

$("#menu-toggle").click(function (e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
});
$(function () {
    $('#form_Search').typeahead({
        source: function (tph, qrs) {
            $('#form_Search').keyup(function (e) {
                $.ajax({
                    url: "/post/find",
                    method: "GET",
                    data: {query: tph},
                    success: function (res) {
                        qrs($.map(res, function (msg) {
                            console.log(msg);
                            if (msg.title != null) {
                                return msg.title;

                            } else {
                                $(location).attr('href', 'https://technology-work.com');
                            }
                        }));
                    },
                    error: function (res) {
                        console.log(res);
                    }
                });
                e.preventDefault();
            });
        }
    });
    $('#form_Search_city').typeahead({
        source: function (tph, qrs) {
            $('#form_Search_city').keyup(function (e) {
                $.ajax({
                    url: "/post/find",
                    method: "GET",
                    data: {query: tph},
                    success: function (res) {
                        qrs($.map(res, function (msg) {
                            console.log(msg);
                            if (msg.city != null) {
                                return msg.city;

                            } else {
                                $(location).attr('href', 'https://technology-work.com');
                            }
                        }));
                    },
                    error: function (res) {
                        console.log(res);
                    }
                });
                e.preventDefault();
            });
        }
    });
    const newItems = $('form.class_form_notify');

    for (let i = 0; i < newItems.length; i++) {
        $(newItems[i]).click(function (e) {
            const $form = $(e.currentTarget);
            const urlPost = Routing.generate('notification_list', {id: this.dataset.postId},true);
            $.ajax({
                url: urlPost,
                withCredentials: true,
                method: 'POST',
                data: $form.serialize(),
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
                },
                success: function (res) {
                    $(location).attr('href', 'https://technology-work.com');
                },
                error: function (res) {

                    toastr.danger("Zresetuj przęglądarkę bądż skontaktuj się z administracją", "Coś poszło nie tak!");
                }
            });
            e.preventDefault();
        });
    }
});
